# -*- coding: utf-8 -*-
"""
Created on Tue May 26 10:49:16 2020

@author: Mickaël Ferreri
"""


import csv
import os
import numpy as np
import sys
from sklearn.metrics import accuracy_score
from sklearn.model_selection import KFold
import pandas as pd
import matplotlib.pyplot as plt
from sklearn.svm import SVC
import skfuzzy as fuzz
from sklearn.metrics import adjusted_rand_score
import random
import sklearn.cluster
import math
from sklearn.ensemble import RandomForestClassifier
from minisom import MiniSom
from sklearn.mixture import GaussianMixture



sys.setrecursionlimit(10000)

path = '../BDD'
path_val = '../BDD_val'
save_path = 'far_db/no_pca'

Do_PCA = False

dic_data_bases = {}
dic_targets = {}
names = []


def printProgressBar (iteration, total, prefix = '', suffix = '', decimals = 1, length = 100, fill = '█'):
    """
    Call in a loop to create terminal srcress bar
    @params:
        iteration   - Required  : current iteration (Int)
        total       - Required  : total iterations (Int)
        prefix      - Optional  : prefix string (Str)
        suffix      - Optional  : suffix string (Str)
        decimals    - Optional  : positive number of decimals in percent complete (Int)
        length      - Optional  : character length of bar (Int)
        fill        - Optional  : bar fill character (Str)
    """
    percent = ("{0:." + str(decimals) + "f}").format(100 * (iteration / float(total)))
    filledLength = int(length * iteration // total)
    bar = fill * filledLength + '-' * (length - filledLength)
    print('\r%s |%s| %s%% %s' % (prefix, bar, percent, suffix), end = '\r')
    # Print New Line on Complete
    if iteration == total: 
        print()

def trunc_name(name):
    """
	Returns the part after a double _ in a string. Use to concatenate the datas from the target and data base files with the same caracteristics
	@params:
	    name        -Required  : the file name, format Data_base__N_clusters_dim_D.csv or target__D_clusters_dim_D.csv (str)
        
    @return:
        The string "N_clusters_dim_D.csv"
	"""
    i = 0
    while name[i:i+2] != '__':
        i+=1
    return(name[i+2:-4])
    
def extract_nb_clust(name):
    """
	Extract the number of clusters in a database given its truncated name (see trunc_name)
	@params:
	    name        -Required  : the file name, format N_clusters_dim_D.csv (str)
        
    @return:
        The number of clusters (int)
	"""
    i=0
    while name[i] != '_':
        i+=1
    return(int(name[:i]))
    

def read_files(path,sep_target=True):
    """
	Reads the files in the path directory
	@params:
	    path        -Required  : the path (str)
        sep_target  -Optional  : default True, set to False if the target (i.e. the cluster of appartenance of each point) 
                                 is in the same file as the coordinates of the points
        
    @return:
        Two dictionnaries containing the data base and the targets. 
        The keys are the structure (format "N_clusters_dim_D"), and
        the values are the targets for eah points or their coordinates 
	"""
    for root, dirs, files in os.walk(path):
        for name in files:
            Data_base = []
            target = []
            print("Reading "+name+"...")
            with open(root+'/'+name, newline='') as csvfile:
                if sep_target:
                    if "Data_base" in name:
                        names.append(trunc_name(name))
                        bddreader = csv.reader(csvfile, delimiter=',')
                        for row in bddreader:
                            l =  np.array(row).astype(float)
                            Data_base.append(l)
                        dic_data_bases[trunc_name(name)] = np.array(Data_base)
                    elif "target" in name:
                        targetreader = csv.reader(csvfile, delimiter=',')
                        for row in targetreader:
                            l = np.array(row).astype(float)
                            target.append(l)
                        dic_targets[trunc_name(name)] = np.array(target)
                else:
                    if "Data_base" in name:
                        names.append(trunc_name(name))
                        bddreader = csv.reader(csvfile,delimiter=',')
                        c = False
                        for row in bddreader:
                            if c:
                                l = np.array(row[1:]).astype(float)
                                Data_base.append(l)
                            c = True
                        Data_base = np.array(Data_base)
                        db, target = Data_base[:,:-1],Data_base[:,-1]
                        for i in range(2,Data_base.shape[1]):
                            dic_data_bases[trunc_name(name)+str(i)] = db[:,:i]
                            dic_targets[trunc_name(name)+str(i)] = target

                        
            print("Done")
    return(dic_data_bases,dic_targets)

#We read the files 
dic_data_bases,dic_targets = read_files(path,True)
dic_val,dic_target_val = read_files(path_val,True)



def distance(x,R):
    """
	Computes the minimal distance between a point and a set of points
	@params:
	    x           -Required  : the point (numpy array of float)
        R           -Required  : the set of points (numpy array)
        
    @return:
        the minimal distance (float)
	"""
    min_dist = np.linalg.norm(x-R[0])
    for y in R[1:]:
        d = np.linalg.norm(x-y)
        if d < min_dist:
            min_dist = d
    return(min_dist)

def distances(DB,R,p):
    """
	Computes the minimal distances between all points in a data set and a set of points elevated to a power p
	@params:
	    DB          -Required  : the data set (numpy array)
        R           -Required  : the set of points (numpy array)
        p           -Required  : the power coefficient (int)
        
    @return:
        the minimal distances list (float list)
	"""
    return([distance(x,R)**p for x in DB])

def calc_R(DB, k, p):
    """
 	Computes the list of centers used for the initialization of the fuzzy c-means algorithm
 	@params:
 	    DB          -Required  : the data set (numpy array)
        k           -Required  : the number of clusters (int)
        p           -Required  : the power coefficient for distance calculation (int)
        
    @return:
        the initial centers (numpy array)
 	"""
    R = [DB[random.randint(0,DB.shape[0]-1)][:]]
    dist_list = distances(DB,R,p)
    sum_dist_p = sum(dist_list)
    while len(R)<k:
        x_picked = False
        while not x_picked:
            rand = random.randint(0,DB.shape[0]-1) 
            dp = dist_list[rand]
            proba = dp/sum_dist_p
            if random.random() >= proba:
                R.append(DB[rand][:])
                x_picked = True
    return(np.array(R))
    

def init_fuzzy_cmeans(DB,target,k,n_iter=10,p_min=0,p_max=10,p_step=1,m=2):
    """
	Initialization of the fuzzy c-mean algorithm. Centers are computed with different hyper-parameters ,
    and the best are kept. The measure of the 'goodness' of a classification is the adjusted rand score
	@params:
	    DB          -Required  : the data set (numpy array)
        target      -Required  : the true label for each point  (numpy array)
        k           -Required  : the estimated number of clusters (int)
        n_iter      -Optionnal : default 10, number of iteration for center calculation, the best is kept
        p_min       -Optionnal : default 0, the minimum power coefficient to test
        p_max       -Optionnal : default 10, the maximum power coefficient to test
        p_step      -Optionnal : default 1, the step for testing p
        m           -Optionnal : default 2, array exponentiation applied to the membership function u_old at each iteration 
                                 in the fuzzy c-means algorithm
        
    @return:
        The initial centers (numpy array)
	"""
    max_score = -1
    for p in np.arange(p_min,p_max,p_step):
        for i in range(n_iter):
            cntr = calc_R(DB, k, p)
            u, _, _, _, _, _  = fuzz.cmeans_predict(DB.T, cntr, m, error=0.001, maxiter=1000)
            pred = np.argmax(u, axis=0)
            score = adjusted_rand_score(pred,target)
            if score > max_score:
                max_score = score
                cntr_ret  = cntr
    return(cntr_ret)

def classify(som, data, class_assignments):
    """Classifies each sample in data in one of the classes definited
    using the method labels_map.
    Returns a list of the same length of data where the i-th element
    is the class assigned to data[i].
    """
    winmap = class_assignments
    default_class = np.sum(list(winmap.values())).most_common()[0][0]
    result = []
    for d in data:
        win_position = som.winner(d)
        if win_position in winmap:
            result.append(winmap[win_position].most_common()[0][0])
        else:
            result.append(default_class)
    return result
    


for name,DB in dic_data_bases.items():

    print(name)
    k = extract_nb_clust(name)
    target = dic_targets[name]
    
    #Shuffling the data base :
    #first, concatenate targets and coordinates
    temp = np.concatenate([DB,target.reshape(len(target),1)],axis=1)
    #shuffling
    np.random.shuffle(temp)
    #separating again
    DB,target = temp[:,:-1],temp[:,-1]
    
    #Extracting the validation set
    DB_val = dic_val[name]
    target_val = dic_target_val[name]
    print('Data base shape : ',DB.shape)
    
    #Number of splits for the k-fold
    n_split=10
    kf = KFold(n_splits=n_split)

    dict_score = { 'single linkage' : [], 
                  'random forest' : [], 
                  'fuzzy c-means' : [], 
                  'SOM' : [],
                  'GMM' : [], 
                  'SVC' : []}  
    mean_hc = 0
    mean_rf = 0
    mean_som = 0
    mean_fuzzy = 0
    mean_gmm = 0
    mean_svc = 0
    
    dict_score_val = { 'single linkage' : 0, 
                  'random forest' : 0, 
                  'fuzzy c-means' : 0, 
                  'SOM' : 0,
                  'GMM' : 0, 
                  'SVC' : 0}   
    
    pca = sklearn.decomposition.PCA(n_components=2)
    pca.fit(DB)
    [val1,val2] = pca.singular_values_
    d1,d2 = val1/(val1+val2),val2/(val1+val2)
    d1 = int(math.sqrt(50)) 
    d2 = int(math.sqrt(50)/math.sqrt(val1/val2)) 
    print('Dimension SOM : ',d1,d2)
    
    if Do_PCA :
        dim = DB.shape[1]
        pca_full = sklearn.decomposition.PCA()
        pca_full.fit(DB)
        pca = sklearn.decomposition.PCA()
        pca.fit(DB)
        n_components_kept = len( (np.where(pca.explained_variance_ratio_<=(1/dim)),pca.explained_variance_ratio_))
        pca = sklearn.decomposition.PCA(n_components=n_components_kept)
        DB = pca.fit_transform(DB)
        DB_val = pca.fit_transform(DB_val)
        print('Dimension after PCA : ', pca.n_components_)
        fig = plt.figure(figsize=[10,5])
        plt.plot(np.cumsum(pca_full.explained_variance_ratio_),color='red',label='Cumul')
        plt.plot(pca_full.explained_variance_ratio_,color='blue',label='Variance')
        plt.axvline(x=pca.n_components_-1,color='black',label='Number of components kept')
        plt.title('Explained variance ratio')
        plt.legend()
        plt.show()
    
    
    #Initialize the srcress bar
    printProgressBar(0, 7*n_split, prefix = 'Progress:', suffix = 'Complete', length = 50)
    count_progress_bar = 0
                
    for train_index, test_index in kf.split(DB):
            
        DB_train, DB_test = DB[train_index,:], DB[test_index,:]
        target_train, target_test = target[train_index], target[test_index]
        
 
        
        clf = SVC(C=1,kernel='poly',degree=2,gamma='scale')
        clf.fit(DB_train,target_train)
        
        count_progress_bar+=1
        printProgressBar(count_progress_bar, 7*n_split, prefix = 'Progress:', suffix = 'Complete', length = 50)
        
        pred = clf.predict(DB_test)
        score = accuracy_score(target_test,pred)
        dict_score['SVC'].append(score)
        mean_svc+=score

        cntr = init_fuzzy_cmeans(DB_train, target_train, k, n_iter = 3,p_min=4,p_max=7)
        u, u0, d, jm, p, fpc  = fuzz.cmeans_predict(DB_test.T, cntr, m=2, error=0.001, maxiter=1000)
        
        count_progress_bar+=1
        printProgressBar(count_progress_bar, 7*n_split, prefix = 'Progress:', suffix = 'Complete', length = 50)
        
        pred = np.argmax(u, axis=0)
        score = adjusted_rand_score(pred,target_test)
        dict_score['fuzzy c-means'].append(score)
        mean_fuzzy+=score
        
        count_progress_bar+=1
        printProgressBar(count_progress_bar, 7*n_split, prefix = 'Progress:', suffix = 'Complete', length = 50)
        
        Gmm = GaussianMixture(n_components=k, covariance_type='full', n_init=10, init_params='kmeans', verbose=0)        
        
        Gmm.fit(DB_train)
        
        count_progress_bar+=1
        printProgressBar(count_progress_bar, 7*n_split, prefix = 'Progress:', suffix = 'Complete', length = 50)
        
        pred = Gmm.fit_predict(DB_test)
        score = adjusted_rand_score(pred,target_test)
        dict_score['GMM'].append(score)
        mean_gmm+=score
        
        #build the model
        HClustering = sklearn.cluster.AgglomerativeClustering(n_clusters=k, linkage="single")
        RF = RandomForestClassifier(n_estimators=30, criterion='entropy', max_features=1,
                                        max_depth=math.sqrt(DB.shape[1]))
            
        som = MiniSom(d1,d2 , DB.shape[1], sigma=1, learning_rate=0.1, 
                              neighborhood_function='gaussian')

        #fit the model on the dataset
        HClustering.fit(DB_train)
        #accuracy of the model
           
        count_progress_bar+=1
        printProgressBar(count_progress_bar, 7*n_split, prefix = 'Progress:', suffix = 'Complete', length = 50)
                       
        #fit the model on the dataset
        RF.fit(DB_train,target_train)
        
        count_progress_bar+=1
        printProgressBar(count_progress_bar, 7*n_split, prefix = 'Progress:', suffix = 'Complete', length = 50)
                          
        som.pca_weights_init(DB_train)
        som.train_batch(DB_train, 1000, verbose=False)  # random training
        class_assignments = som.labels_map(DB_train, target_train)
           
           
        pred = HClustering.fit_predict(DB_test)
        score = adjusted_rand_score(pred,target_test)
        dict_score['single linkage'].append(score)
        mean_hc+=score
        u, u0, d, jm, p, fpc  = fuzz.cmeans_predict(DB_test.T, cntr, m=2, error=0.001, maxiter=1000)
        
        pred = RF.predict(DB_test)
        dict_score['random forest'].append(RF.score(DB_test,target_test))
        mean_rf+=RF.score(DB_test,target_test)
        
        score_som = accuracy_score(target_test, classify(som, DB_test, class_assignments))
        dict_score['SOM'].append(score_som)
        mean_som+=score_som
        
        count_progress_bar+=1
        printProgressBar(count_progress_bar, 7*n_split, prefix = 'Progress:', suffix = 'Complete', length = 50)

    pred = clf.predict(DB_val)
    score = accuracy_score(target_val,pred)
    dict_score_val['SVC'] = score
    
    print('Validation SVC :',dict_score_val)
    print('Score moyen SVC : ',mean_svc/n_split)
    
    
    u, u0, d, jm, p, fpc  = fuzz.cmeans_predict(DB_val.T, cntr, m=2, 
                                                error=0.001, maxiter=100000)
    cluster_membership = np.argmax(u, axis=0)

    score = adjusted_rand_score(target_val.ravel(),cluster_membership)
    print('Validation fuzzy c-means :',score)
    print('Score moyen fuzzy c-means : ',mean_fuzzy/n_split)


    
    pred = HClustering.fit_predict(DB_val)
    score = adjusted_rand_score(target_val.ravel(),pred)
    print('Validation single linkage :',score)
    print('Score moyen single linkage : ',mean_hc/n_split)
    
    pred = RF.predict(DB_val)
    print('Validation random forest : ',RF.score(DB_val,target_val))
    print('Score moyen random forest : ',mean_rf/n_split)

    
    score_som = accuracy_score(target_val, classify(som, DB_val, class_assignments))
    print('Validation SOM :', score_som)
    print('Score moyen SOM : ',mean_som/n_split)

    score_gmm = adjusted_rand_score(target_val.ravel(),Gmm.fit_predict(DB_val))
    print('Validation GMM : ',score_gmm)
    print('Score moyen GMM : ',mean_gmm/n_split)
    
    
    df = pd.DataFrame(dict_score)
    print(df)
    fig = plt.figure(figsize=[10,5])    
    
    df.boxplot()
    
    plt.show()
    
    script_dir = os.path.dirname(__file__)
    results_dir = os.path.join(script_dir+'Resultats/k_known/'+save_path)
                           
    fig.savefig(script_dir+'Comparaison_resultats_'+name+'.png')
