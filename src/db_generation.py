# -*- coding: utf-8 -*-
"""
Created on Wed Feb 26 10:57:32 2020

@author: Mickaël Ferreri
"""

import random
import numpy as np
import matplotlib.pyplot as plt
from itertools import combinations,combinations_with_replacement
import matplotlib.colors
import sklearn.decomposition
import math
random.seed()

def gram_schmidt_columns(X):
    """
	Return the orthogonalized matrix of X 
	@params:
	    X           -Required  : the matrix to orthogonalized
        
    @return:
        A numpy array that is the orthogonalized matrix of X
	"""
    Q, R = np.linalg.qr(X)
    return Q

def gen_new_corr_var(var,noise):
    """
	Generate a vector correlated to var by adding gaussian noise
	@params:
	    var         -Required  : the original vector 
	    noise       -Required  : the level of noise (variance of the gaussian generated)
        
    @return:
        A numpy array that is the generated vector
	"""
    d = len(var)
    if noise<0:
        return(np.array([-var[i]+random.normalvariate(0,-noise) for i in range(d)]))
    else:
        return(np.array([var[i]+random.normalvariate(0,noise) for i in range(d)]))
        

def is_out(u,center,cov):
            d = len(center)
            l = [i for i in range(d)]
            dist = [abs(u[i]-center[j])  for (i,j) in combinations(l,2)]
            
            for k,(i,j) in enumerate(combinations(l,2)):
                if dist[k]>3*cov[i,j]:
                    return(True)
            return(False)
                
        
def add_outliers(data,center,cov,n):
    """
	Replace points of a data set by outliers. Points are considered as outliers 
    if they are farther than 3*sigma of the center. 
    This function generate outliers that follows a reducted gaussian law.
	@params:
	    data        -Required  : the data set (matrix)
	    center      -Required  : the center of the gaussian (float list, size = dimension)
	    cov         -Required  : the covariance matrix used to generate the data set
        n           -Required  : the number of outliers
        
    @return
        None
	"""
    
    D = cov/np.diag(cov)
    count = 0
    np.random.shuffle(data)
    n_gen = 10000
    outl = []
    while count<n:
        u = np.random.multivariate_normal(center, D, n_gen)
        for l in u:
           outl.append(is_out(l,center,cov))
 
        for i in range(n_gen):
            if outl[i] and count<n:
                count+=1
                data[:,i] = u[i]
            elif count==n:
                return(None)  

    
    
    

def generate_gaussian(size, center, corr, corr_deg, outliers, size_fac = 1, plot = False):
    """
	Generates a gaussian
	@params:
	    size        -Required  : the number of generated points (int)
	    center      -Required  : the center of the gaussian (float list)
	    corr        -Required  : determine the correlations between the variables. 
                                 list of lists of size the number of groups of correlated variables
                                 values are the list of the correlated variables 
        corr_deg    -Required  : the degree of correlation in each group (int list, size = len(corr), 
                                 value between -1 and 1, 0 = very correlated, if <0, anti-correlation )
        outliers    -Required  : the fraction of outliers (float, between 0 and 1)
        size_fac    -Optionnal : Default 1, factor to multiply the variance of the gaussian (float)
        plot        -Optionnal : Default False, if True, plots the generated data set
        
    @return:
        A numpy array that is the generated data set
	"""
    
    dimension = len(center)
    var_list = [[] for i in range(dimension)]
    X = np.zeros((dimension,dimension))
    
    for i in range(dimension):
        var_list[i] = np.random.rand(dimension)
        
    ort_var_list = gram_schmidt_columns(var_list)
    
    for i,l in enumerate(corr):
        for j,v in enumerate(l):
                v = int(v)
                if j==0:
                    vec = ort_var_list[i,:]
                    X[:,v]=vec
                else:
                    vec2 = gen_new_corr_var(vec,corr_deg[i])
                    X[:,v]=np.array(vec2)
    
    
    X_corr = np.corrcoef(X.T)
    X_corr = X_corr.T
    std = size_fac*np.eye(dimension)
   
    cov = std.dot(std.T).dot(X_corr)
            
    l_plot = np.random.multivariate_normal(center, cov, size).T
    add_outliers(l_plot,center,cov,int(outliers*size))

    
    
    if plot:
        for i in range(dimension):
            for j in range(dimension):
                if i<j:
                    print(i,j)
                    plt.plot(l_plot[i],l_plot[j],'x')
                    plt.axis('equal')
                    plt.show()
    return(l_plot)

    
    
    
def normalize(Data_base):
    """
	Normalizes a data base of shape (i,j,k)
	@params:
	    Data_base   -Required  : the data base to normalizes (numpy array)
        
    @return:
        A numpy array that is the normalized data base 
	"""
    grav_center = np.mean(np.mean(Data_base,axis=0),axis=1)
    for i in range(Data_base.shape[0]):
        for j in range(Data_base.shape[2]):
            Data_base[i,:,j]-= grav_center

    Data_base/=np.std(Data_base)
    return(Data_base)
    
    

def gen_data_base(N_point,D,centers,outliers,size_fac, plot = False):
    """
	Generates a data base with a mixture of gaussians
	@params:
	    N_point     -Required  : the number of generated points for each gaussian (int list)
	    D           -Required  : the dimension of the gaussians (int)
	    centers     -Required  : the centers of the gaussian (float list list)
        outliers    -Required  : the fraction of outliers for each gaussian (float list)
        size_fac    -Required  : factor list to multiply the variance of each of the gaussian (float list)
        plot        -Optionnal : Default False, if True, plots the generated data base
        
    @return:
        A numpy array that is the generated data base 
	"""
    
    N_gauss = len(N_point)
    Data_base = []
    
    #We take 2 groups of 2 correlated variables, the others are uncorrelated
    #If dimension D<4, no variables are correlated to another
    if D<4:
        corr = [[i] for i in range(D)]
        corr_deg = [1 for i in range(D)]
    else:
        choose = [i for i in range(D)]
        np.random.shuffle(choose)
        v1,v2,v3,v4 = choose[0],choose[1],choose[2],choose[3]
        corr = [[v1,v2],[v3,v4]]
        corr_deg = [2*np.random.rand(1)[0]-1,2*np.random.rand(1)[0]-1]
        for i in range(D):
            if i not in [v1,v2,v3,v4]:
                corr.append([i])
                corr_deg.append(1)

    for i in range(N_gauss):
        gauss = generate_gaussian(N_point[i],centers[i],corr,corr_deg,
                                           outliers[i],size_fac=size_fac[i])
        Data_base.append(gauss)
        print(gauss.shape)
    Data_base = normalize(np.array(Data_base))

    if plot:
        for i in range(D):
            for j in range(D):
                if i<j:
                    print(i,j)
                    for k in range(N_gauss):
                        plt.plot(Data_base[k,i,:],Data_base[k,j,:],'x')
                    plt.axis('equal')
                    plt.show()
        
    return(Data_base)



def generate_non_gaussian_clust(size,center,l_fun):
    """
	Generates non gaussian clusters
	@params:
	    size        -Required  : the number of generated points (int)
	    center      -Required  : the center of the cluster (float list)
        l_fun       -Required  : the list of functions used to generate the cluster

    @return:
        A numpy array that is the generated cluster
	"""
    d = len(center)
    x = np.random.rand(size)
    clust = [x+center[0]]
    dimension = len(center)
    for i in range(1,dimension):
        new_x = l_fun[i](x)
        new_x = new_x*( 1 + ((np.random.rand(size)-0.5)/5)) + center[i]
        clust.append(new_x)
    clust = np.array(clust)    
    
    return(clust)


def generate_non_gaussian_database(N_point,centers,l_fun):
    """
	Generates a non gaussian data base
	@params:
	    N_point     -Required  : the number of generated points for each cluster (int list)
	    centers     -Required  : the centers of each cluster (float list list)
        l_fun       -Required  : the list of functions used to generate the clusters

    @return:
        A numpy array that is the generated data base
	"""
    N_clust = len(N_point)
    Data_base = []
    
    for i in range(N_clust):
        Data_base.append(generate_non_gaussian_clust(N_point[i],centers[i],l_fun))
        
    Data_base = normalize(np.array(Data_base))
        
    return(Data_base)
    
def gen_indep_var(x):
    """
	Generates a new variable x2, orthogonal to x
	"""
    x2 = np.random.rand(len(x))
    mat = np.concatenate(([x],[x2]),axis=0).T
    x2 = gram_schmidt_columns(mat)[:,1]
    return(x2)

def sin_exp(x):
    return(np.sin(np.exp(x)))
    
def is_far(l,dist):
    """
	Compute the distance between points in l and tell if at least one distance is < dist
	@params:
	    l           -Required  : the list of points (float list list)
	    dist        -Required  : the minimal distance between two points (float)
        
    @return:
        True if all points in l are farthest from each other than dist, else False
	"""
    for i,c1 in enumerate(l):
        for j,c2 in enumerate(l):
            if i!=j and np.linalg.norm(c1-c2) < dist:
                return(False)
    return(True)
        

def gen_centers(N,D,dist,inf,sup):
    """
	Generate N points of dimension D, distant from each other from dist, 
    and with coefficients between inf and sup
	@params:
	    N           -Required  : the number of points (int)
	    D           -Required  : the dimension of the points (int)
        dist        -Required  : the minimal distance between two points (float)
        inf         -Required  : the minimal coeff
        sup         -Required  : the maximal coeff
        
    @return:
        A numpy array containing the N points generated
	"""
    random.seed()
    c1 = np.array([random.randrange(inf,sup) for k in range(D)])
    centers = [c1]
    test = [c1]
    for i in range(N-1):
        c2 = np.array([random.randrange(inf,sup) for k in range(D)])
        test.append(c2)
        while not is_far(np.array(test),dist):
            test.pop()
            c2 = np.array([random.randrange(inf,sup) for k in range(D)])
        centers.append(c2)
    return(np.array(centers))
    
def gen_center_grid(N,D):
    """
	Generate N points of dimension D in the grid [-3,0,3]^D
	@params:
	    N           -Required  : the number of points (int)
	    D           -Required  : the dimension of the points (int)
        
    @return:
        A numpy array containing the N points generated
	"""
    permut = list((combinations_with_replacement([0,-1,1],D)))
    np.random.shuffle(permut)
    c1 = np.array(permut[0])
    centers = [c1]
    for i in range(N-1):
        c2 = np.array(permut[i])
        centers.append(c2)
    return(np.array(centers))
    
def gen_name(N,D,txt="Data_base",direc='temp'):
    """
	Generate a name for the data base
	@params:
	    N           -Required  : the number of clusters (int)
	    D           -Required  : the dimension of the space (int)
        
    @return:
        The string "Data_base_N_clusters_dim_D.csv"
	"""
    return("./"+direc+"/"+txt+"__"+str(N)+"_clusters_dim_"+str(D)+".csv")
    
def create_target(Data):
    """
	Separates a data base in targets and values
	@params:
	    Data        -Required  : the data base of shape (n_cluster,n_point,dimension)  (numpy array)

    @return:
        A tuple (target,values) (tuple of numpy arrays)
	"""
    target = []
    values = []
    for i,cluster in enumerate(Data):
        for point in cluster.T:
            target.append(i)
            values.append(point)
    return(target,np.array(values))
    

#We generate several data bases, with different number of clusters and differents dimension
    
#The list of the numbers of clusters [5,10] [3,5,7,20]
N_clust = [20]
#We generate 1000 points per cluster
N_point = [[int(10000/n) for i in range(n)] for n in N_clust]

#The list of dimensions  [10,30] [5,10,15,20]
D = [100]

#We generate the centers
centers = np.array([[gen_center_grid(len(N_point[i]),D[j]) for j in range(len(D))]
            for i in range(len(N_point))])
print(centers.shape)

#For each group of cluster, we give arbitrary outliers percentage and size factor

"""
outliers = [[0,0.1,0.2],
            [0,0.2,0.1,0.15,0.5],
            [0,0.1,0.2,0.15,0.1,0.3,0.5],
            [0,0,0.2,0.2,0.2,0.2,0.1,0.1,0.5,0.5,0.75,0,0.1,0.2,0.15,0.3,0.25,0.1,0,0.1]]
size_fac = [[1,1.5,0.5],
            [1,1.2,0.75,2,0.4],
            [1,1,0.4,1.5,2,0.5,1.25],
            [1,1,1,1,2,0.5,0.5,1,3,0.1,1,1,1,1,0.3,0.4,0.6,1,1.5,0.4]]

outliers = [[random.uniform(0,0.75) for i in range(n)] for n in D]
size_fac =  [[random.uniform(0.1,5) for i in range(n)] for n in D]
"""


#Set plot = True to plot the generated data bases
plot = False
#A list of color to plot the clusters
color = ['blue','orange','green','yellow','lime','red','purple','pink','brown','silver',
         'olive','cyan','maroon','magenta','black','darkcyan',
         'turquoise','gold','sandybrown','darkviolet']

#The data bases generation itself :
for i in range(len(N_point)):
    print("i : ",i)
    for j in range(len(D)):
        print("j : ",j)
        l_fun = []
        for k in range(D[j]):
            if not i%3:
                l_fun.append(np.log)
            elif not i%4:
                l_fun.append(sin_exp)
            else:
                l_fun.append(gen_indep_var)
        #DB = gen_data_base(N_point[i],D[j],centers[i,j],outliers[i],size_fac[i],plot)
        DB = generate_non_gaussian_database(N_point[i],centers[i,j],l_fun)
        for k in range(len(N_point[i])):
            plt.plot(DB[k,0,:],DB[k,1,:],'x')
        plt.axis('scaled')
        plt.show()  
        
        target,DB = create_target(DB)
        print(DB.shape)
        
        pca = sklearn.decomposition.PCA(n_components=2)
        proj = pca.fit_transform(DB)  
        
        fig = plt.figure(figsize=(8,8))
        plt.scatter(proj[:,0], proj[:,1], c=target, cmap=matplotlib.colors.ListedColormap(color))
        cb = plt.colorbar()
        plt.title(gen_name(len(N_point[i]),D[j]))
        plt.axis('scaled')
        plt.show()
        np.savetxt(gen_name(len(N_point[i]),D[j]), DB, delimiter=',')
        np.savetxt(gen_name(len(N_point[i]),D[j],txt="target"), target, delimiter=',')


for i in range(len(N_point)):
    print("i : ",i)
    for j in range(len(D)):
        print("j : ",j)
        #DB = gen_data_base(N_point[i],D[j],centers[i,j],outliers[i],size_fac[i],plot)
        #The list of functions we are going to use in the non gaussian database
        l_fun = []
        for k in range(D[j]):
            if not i%3:
                l_fun.append(np.log)
            if not i%4:
                l_fun.append(gen_indep_var)
            else:
                l_fun.append(sin_exp)
        DB = generate_non_gaussian_database(N_point[i],centers[i,j],l_fun)
        for k in range(len(N_point[i])):
            plt.plot(DB[k,0,:],DB[k,1,:],'x')
        plt.axis('scaled')
        plt.show()  
        
        target,DB = create_target(DB)
        print(DB.shape)
        
        pca = sklearn.decomposition.PCA(n_components=2)
        proj = pca.fit_transform(DB)
        
        fig = plt.figure(figsize=(8,8))
        plt.scatter(proj[:,0], proj[:,1], c=target, cmap=matplotlib.colors.ListedColormap(color))
        plt.title(gen_name(len(N_point[i]),D[j], direc='temp_val'))
        cb = plt.colorbar()
        plt.axis('scaled')
        plt.show()
        np.savetxt(gen_name(len(N_point[i]),D[j], direc='temp_val'), DB, delimiter=',')
        np.savetxt(gen_name(len(N_point[i]),D[j],txt="target", direc='temp_val'), target, delimiter=',')





